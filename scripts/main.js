/*
Responsive Navigation
*/

$(document).ready(function() {
  $('body').addClass('js');
  var $menu = $('.site-nav-container'),
    $menulink = $('.menu-link'),
    $menuTrigger = $('.menu-item-has-children > a'),
    $searchLink = $('.search-link'),
    $siteSearch = $('.search-module'),
    $siteWrap = $('.site-wrap');

  $searchLink.click(function(e) {
    e.preventDefault();
    $searchLink.toggleClass('active');
    $siteSearch.toggleClass('active');
    $('#search-site').focus();
  });

  $menulink.click(function(e) {
    e.preventDefault();
    $menulink.toggleClass('active');
    $menu.toggleClass('active');
    $siteWrap.toggleClass('nav-active');
  });

  $('.menu-item-has-children').append("<span class='m-subnav-arrow'></span>");

  $('.m-subnav-arrow').click(function() {
    $(this).toggleClass('active');
    var $this = $(this).prev(".sn-level-2");
    $this.toggleClass('active').next('ul').toggleClass('active');
  });

});

/*
Sticky Nav
*/

$(function() {
  //Set the height of the sticky container to the height of the nav
  //var navheight = $('.site-nav-container').height();
  // grab the initial top offset of the navigation 
  var sticky_navigation_offset_top = $('.sh-sticky-wrap').offset().top;

  // our function that decides weather the navigation bar should have "fixed" css position or not.
  var sticky_navigation = function() {
    var scroll_top = $(window).scrollTop(); // our current vertical position from the top

    // if we've scrolled more than the navigation, change its position to fixed to stick to top,
    // otherwise change it back to relative
    if (scroll_top > sticky_navigation_offset_top) {
      $('.sh-sticky-wrap').addClass('stuck');
      //$('.sh-sticky-wrap').addClass('stuck').css('height',navheight);
    } else {
      $('.sh-sticky-wrap').removeClass('stuck');
    }
  };

  // run our function on load
  sticky_navigation();

  // and run it again every time you scroll
  $(window).scroll(function() {
    sticky_navigation();
  });

});

/*
Custom Hero Slider
*/

$(document).ready(function() {

  function nextSlide(destination) {
    if (!destination) {
      var currentSlide = $('.site-header-wrap').attr('data-slide');
      var addOne = Number(currentSlide) + 1;
    } else {
      var addOne = destination;
    }

    if (addOne > 4) {
      var slideNum = 1;
    } else {
      var slideNum = addOne;
    }

    // Show active slide & hide inactive slides 
    $('.si-content').hide();
    $('.slide-' + slideNum).show();

    // Activate current slide dot & unactivate all other dots 
    $('.dot').removeClass('active');
    $('.dot-' + slideNum).addClass('active');

    // Set background image & update data attribute
    $('.site-header-wrap').attr('data-slide', slideNum);
    $('.site-header-wrap').css('background-image', 'url(assets/img/bg-for-slide-' + slideNum + '.jpg');
  }

  // Next Slide (Arrow) Trigger
  $('.next').on('click', function() {
    nextSlide();
  })

  // Jump To (dots) Trigger
  $('.dot').on('click', function() {
    var jumpTo = $(this).attr('data-slide');
    nextSlide(jumpTo);
  })

  $('.prev').on('click', function() {
    var currentSlide = $('.site-header-wrap').attr('data-slide');
    var addOne = Number(currentSlide) - 1;

    if (addOne < 1) {
      var slideNum = 4;
    } else {
      var slideNum = addOne;
    }

    $('.si-content').hide();
    $('.slide-' + slideNum).show();

    $('.dot').removeClass('active');
    $('.dot-' + slideNum).addClass('active');

    $('.site-header-wrap').attr('data-slide', slideNum);
    $('.site-header-wrap').css('background-image', 'url(assets/img/bg-for-slide-' + slideNum + '.jpg');

  })

  // slider timed rotation
  var timer = setInterval(nextSlide, 5000);
  $('.site-intro').hover(function(ev) {
    clearInterval(timer); // stop rotation on hover
  }, function(ev) {
    timer = setInterval(nextSlide, 5000);
  });

})

